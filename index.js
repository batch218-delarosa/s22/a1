/*
    Create functions which can manipulate our arrays.
*/

// console.log("Testing")

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function registerUser(newUser) {
    let isExistingUser = registeredUsers.indexOf(newUser);

    if (isExistingUser != -1) {
        alert("Registration failed. Username already exists!");
        console.warn(`Attempted to register ${newUser} but user already exits.`);
    } else {
        registeredUsers.push(newUser);
    }
}

/*registerUser("James Jeffries");
registerUser("Bobby Fischer");


console.log(registeredUsers);
    */


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(existingUser) {
    let isExistingUser = registeredUsers.indexOf(existingUser);

    if (isExistingUser == -1) {
        alert("User not found.");
        console.warn(`User ${existingUser} is not an existing user.`)
    } else {
        friendsList.push(existingUser);
        alert(`You have added ${existingUser} as a friend!`);        
    }
}

/*console.info("Add an unregistered user as a friend.");
addFriend("Tina Moran"); 
console.log("Add Bobby Fischer as friend.");
addFriend("Bobby Fischer");

console.log(friendsList);*/





/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function showArrayItems(array) {
    if (array.length == 0) {
        alert("You currently have 0 friends. Add one first.");
        console.warn("You currently have 0 friends. Add one first.");
    } else {
        array.forEach(value => {
            console.log(value);
        });
    }
}

/*console.info("Try to show items of registeredUsers");
showArrayItems(registeredUsers); 
console.info("Try to show items of friendsList");
showArrayItems(friendsList); 
console.info("Try to show items of empty array");
showArrayItems([]); */




/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


function howManyFriends(array) {
    if (array.length == 0) {
        alert("You currently have 0 friends. Add one first.");
    } else  {

        array.length == 1 
        ? alert(`You currently have ${array.length} friend.`)
        : alert(`You currently have ${array.length} friends.`)
        
    }
}

/*howManyFriends(friendsList);
howManyFriends(registeredUsers);
howManyFriends([]);*/



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function deleteLastUserFromFriendsList() {
    if (friendsList.length == 0 ) {
        alert("You currently have 0 friends. Add one first.");
        console.warn("You currently have 0 friends. Add one first.");
    } else {
        friendsList.pop();
    }
}

/*console.info("Try deleting from friendsList");
deleteLastUserFromFriendsList();
console.log("Show friends list");
console.log(friendsList);
console.info("Try deleting from friendsList again");
deleteLastUserFromFriendsList();*/

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/



function deleteFriend(user) {
    let friendIndex = friendsList.indexOf(user);
    let isAFriend = friendsList.includes(user);

    if (friendsList.length == 0 ) {
        alert("You currently have 0 friends. Add one first.");
        console.warn("You currently have 0 friends. Add one first.");
    } else if (isAFriend) {
        friendsList.splice(friendIndex, 1);
    } else {
        alert(`User ${user} is registered but not a friend`);
        console.warn(`User ${user} is registered but not a friend`);
    }

}


/*console.log("Try deleting Macie West from friendsList");
deleteFriend("Macie West");



friendsList = registeredUsers;
console.log("Populate friendsList with registeredUsers")
console.log(friendsList);


console.log("Delete Macie West from friendsList");
deleteFriend("Macie West");

console.log("Show friendsList");
console.log(friendsList);

console.log("Delete Macie West from friendsList again");
deleteFriend("Macie West");

console.log("Show friendsList");
console.log(friendsList);
*/
